# -- Project information
project = "pyconjp-2021"
copyright = "2021, Kazuya Takei"
author = "Kazuya Takei"
release = "2021.8"

# -- General configuration
language = "en"
extensions = [
    "sphinxcontrib.blockdiag",
    "sphinxcontrib.sass",
    "sphinxemoji.sphinxemoji",
    "sphinx_revealjs",
]
template_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store", ".venv", "venv", "var", "_sections"]

# -- Options for Revealjs output
revealjs_css_files = [
    "revealjs4/plugin/highlight/zenburn.css",
]
revealjs_static_path = ["_static"]
revealjs_style_theme = "css/mytheme.css" 
revealjs_script_conf = """
{
    controls: false,
    hash: true,
    center: false,
    transition: 'none',
}
"""
revealjs_script_plugins = [
    {
        "src": "revealjs4/plugin/notes/notes.js",
        "name": "RevealNotes",
    },
    {
        "src": "revealjs4/plugin/highlight/highlight.js",
        "name": "RevealHighlight",
        "options": """
            {async: true, callback: function() { hljs.initHighlightingOnLoad(); } }
        """
    },
    {
        "src": "js/title.js",
    }
]

sass_src_dir = "_sass"
sass_out_dir = "_static/css"
sass_targets = {
    "mytheme.scss": "mytheme.css"
}

blockdiag_html_image_format = "SVG"
