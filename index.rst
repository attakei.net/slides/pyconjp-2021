========================================
PyPIデビュー手前の人のための地下活動手法
========================================

PyPICloudを使ったプライベートレジストリの構築

:date: 2021-10-15
:event: PyCon JP 2021
:speaker: Kazuya Takei / @attakei / NIJIBOX Co., Ltd.
:hashtag: `#pyconjp <https://twitter.com/hashtag/pyconjp>`_

.. include:: _sections/1-introduction.rst

.. include:: _sections/2-distribute-packages.rst

.. include:: _sections/3-about-pypicloud.rst

.. include:: _sections/4-using-pypicloud.rst

.. include:: _sections/5-used-pypicloud.rst

.. include:: _sections/6-conclusion.rst
